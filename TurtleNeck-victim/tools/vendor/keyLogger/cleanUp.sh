#!/bin/bash

echo "Make sure you executed this script with sudo or root account"
echo "removing Terimnal app in Accessibility to monitor systeme ..."
python tccutil.py -r com.apple.Terminal
echo "Done !"
echo ""
echo "removing Python app in Accessibility to monitor systeme ..."
python tccutil.py -r org.python.python
echo "Done !"

PID=$!
echo "Launched with PID $PID"
echo
echo "Press enter to finish"
echo

read -n 1 -s -p ""

kill $PID
